<?php
session_start();
$_SESSION['tema']=" ";
if(isset($_POST['tema'])){
    $_SESSION['tema']=$_POST['tema'];
}


switch($_SESSION['tema']){
    case 'azul': 
        echo "<link rel=\"stylesheet\" href=\"CSS/azul.css\" />";
        break;
    case 'morado':
        echo "<link rel=\"stylesheet\" href=\"CSS/morado.css\" />";
        break;
    case 'naranja':
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
    default:
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title>Práctica 1</title>
        <link rel="stylesheet" href="CSS/estilos.css" />
    </head>
    <body>
        <div id="wrapper">
            <header id="cabecera">
                <?php
                include 'include/cabecera.php';
                ?>
            </header>
            <br>
            <nav id="menu">
                <?php
                include 'include/menu.php';
                ?>
            </nav>
            <br>
            <section id="contenido">

                <?php
                isset($_GET['seccion'])?$seccion=$_GET['seccion']:$seccion="inicio";

                if($seccion=="inicio"){
                    include 'include/contenidoInicio.php';
                }else if($seccion=="datos"){
                    include 'include/contenidoDatos.php';
                }else if($seccion=="contacto"){
                    include 'include/contenidoContacto.php';
                }else{
                    include 'include/contenidoLogin.php';
                }

                ?>
            </section>

            <footer id="pie">
                <p>
                    <?php
                    include 'include/pie.php'
                    ?>
                </p>
            </footer>
        </div>
    </body>
</html>