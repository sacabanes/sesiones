<?php
    session_start();
?>
    <form method="post" action="../Formulario/include/procesaFormulario.php">

        <label for="nombre_usuario">Dínos tu nombre</label><br/>
        <input type="text" id="nombre_usuario" name="nombre" />
        <br/>
        <br/>
        <label for="edad_usuario">¿Cuántos años tienes?</label><br/>
        <input type="text" id="edad_usuario" name="edad" />
        <br/>
        <br/>
        <br/>
        <label for="tema">Ellige un tema:</label><br/>
        <input type="radio" name="tema" value="azul"/>Azul<br/>
        <input type="radio" name="tema" value="morado"/>Morado<br/>
        <input type="radio" name="tema" value="negro"/>Negro<br/>
        <input type="radio" name="tema" value="naranja"/>Naranja<br/>
        <br/>
        <br/>
        <br/>

        <label for="generos">¿Cuáles son tus géneros literarios favoritos?</label><br/>
        <input type="checkbox" name="generos" value="Fantasía"/>Fantasía<br/>
        <input type="checkbox" name="generos" value="Juvenil"/>Juvenil<br/>
        <input type="checkbox" name="generos" value="Policial (Thriller)"/>Policial (Thriller)<br/>
        <input type="checkbox" name="generos" value="Aventura"/>Aventura<br/>
        <input type="checkbox" name="generos" value="Terror"/>Terror<br/>
        <input type="checkbox" name="generos" value="Ciencia Ficción"/>Ciencia Ficción<br/>
        <input type="checkbox" name="generos" value="Histórico"/>Histórico<br/>
        <input type="checkbox" name="generos" value="Romántico"/>Romántico<br/>
        <input type="checkbox" name="generos" value="Contemporáneo"/>Contemporáneo
        
        <br/>
        <br/>
        <br/>

        <label for="autores">¿Qué autores te gustan más de esta lista?</label><br/>
        <input type="checkbox" name="autores[]" value="Carlos Ruiz Zafón"/>Carlos Ruiz Zafón<br/>
        <input type="checkbox" name="autores[]" value="Virginia Woolf"/>Virginia Woolf<br/>
        <input type="checkbox" name="autores[]" value="Stephen King"/>Stephen King<br/>
        <input type="checkbox" name="autores[]" value="Agatha Christie"/>Agatha Christie<br/>
        <input type="checkbox" name="autores[]" value="H. P. Lovecraft"/>H. P. Lovecraft<br/>
        <input type="checkbox" name="autores[]" value="Jane Austen"/>Jane Austen<br/>
        <input type="checkbox" name="autores[]" value="Neil Gaiman"/>Neil Gaiman<br/>
        <input type="checkbox" name="autores[]" value="Isabel Allende"/>Isabel Allende<br/>

        <br/>
        <br/>
        <br/>
        <br/>

        <label for="tamano">¿Te gustan los libros densos o prefieres los cortos?</label><br/>
        <input type="radio" name="tamano" value="Interminables"/>¡Interminables!<br/>
        <input type="radio" name="tamano" value="Microscópicos"/>Microscópicos

        <br/>
        <br/>
        <br/>
        <br/>

        <label for="libros">De todos los libros que se muestran en la pestaña desplegable, ¿cuál te gustaría leer?</label><br/>
        <select id="libros" name="libros">
            <option value="" selected="selected">*Ábreme*</option>
            <option value="El Hobbit - J.R.R Tolkien">El Hobbit - J.R.R Tolkien</option>
            <option value="Harry Potter y la piedra filosofal - J.K.Rowling">Harry Potter y la piedra filosofal - J.K.Rowling</option>
            <option value="Los hombres que no amaban a las mujeres - Stieg Larsson">Los hombres que no amaban a las mujeres - Stieg Larsson</option>
            <option value="Viaje al centro de la Tierra - Jules Verne">Viaje al centro de la Tierra - Jules Verne</option>
            <option value="El resplandor - Stephen King">El resplandor - Stephen King</option>
            <option value="Fundación - Isaac Asimov">Fundación - Isaac Asimov</option>
            <option value="Los pilares de la tierra - Ken Follet">Los pilares de la tierra - Ken Follet</option>
            <option value="Forastera - Diana Gabaldon">Forastera - Diana Gabaldon</option>
            <option value="El amante japonés - Isabel Allende">El amante japonés - Isabel Allende</option>

        </select>

        <br/>
        <br/>
        <br/>
        <br/>

        <p id="opinion">En el caso de que ya hayas leído alguno, cuéntanos tu opinión</p>

        <label for="titulo"></label>
        <input type="text" id="titulo" name="titulo" value="" placeholder="Título del libro"/><br/>
        <label for="opinion_usuario"></label>
        <textarea id="opinion_usuario" name="opinion_usuario" cols="40" rows="5"></textarea>
        
        <br/>
        <br/>
        <br/>
        <br/>
        
        <p id="cantidad">¿Cuántos libros has leído este año?</p><br/>
        
        <input type="number" id="cantidad" name="cantidad" value=""/>
        
        <br/>
        <br/>
        <br/>
        <br/>
        
        <p id="valoracion">Valora la página</p><br/>
        
        <input type="number" id="valoracion" name="valoracion" value="" min="0" max="5"/>
        
        <br/>
        <br/>
        <br/>
        <br/>
        
        <p id="comentario">¿Quieres hacer algún comentario acerca de la página?</p><br/>
        
        <label for="comentario"></label>
        <textarea id="comentario" name="comentario" cols="40" rows="1"></textarea>
        
        <br/>
        <br/>
        <br/>
        <br/>

        <input type="submit" name="enviar" value="¡Envíame!"/>

        <br/>
        <br/>
        <br/>
        <br/>

    </form>