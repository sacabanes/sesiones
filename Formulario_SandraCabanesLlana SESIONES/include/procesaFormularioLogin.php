<?php
session_start();

$estilos=$_SESSION['tema'];


switch($estilos){
   case 'azul': 
        echo "<link rel=\"stylesheet\" href=\"CSS/azul.css\" />";
        break;
    case 'morado':
        echo "<link rel=\"stylesheet\" href=\"CSS/morado.css\" />";
        break;
    case 'naranja':
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
    default:
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
        
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <link  rel="stylesheet" href="../CSS/estils.css" />
    </head>
    <body>
        <p>
            <?php

            if($_GET['nom']==""){
                $nom="sin valor";
            }else{
                $nom=$_GET['nom'];
                $_SESSION['usuario']=$nom;
            }
            if($_GET['pass']==""){
                $pass="sin valor";
            }else{
                $pass=$_GET['pass'];
            }

            $encontrado=false;

            $fichero=fopen("../contrasenya.txt","r");

            while (!feof($fichero) && !$encontrado){
                $linea=fgets($fichero);
                $array_usuarios=explode(':',$linea);

                if($array_usuarios[0]==$nom && $array_usuarios[1]==$pass){
                    header("Location: ../indice.php?seccion=inicio");
                    $encontrado=true;
                    die();
                    exit;
                }
            }

            if(!$encontrado){
                $_SESSION['usuario']="Login incorrecto";
                header("Location: ../indice.php?seccion=login");
                die();
            }

            fclose($fichero);
            ?>

            <a href="../indice.php">volver a inicio</a>

        </p>
    </body>
</html>

