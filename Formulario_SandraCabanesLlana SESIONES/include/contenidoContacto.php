<?php


$estilos=$_SESSION['tema'];

switch($estilos){
   case 'azul': 
        echo "<link rel=\"stylesheet\" href=\"CSS/azul.css\" />";
        break;
    case 'morado':
        echo "<link rel=\"stylesheet\" href=\"CSS/morado.css\" />";
        break;
    case 'naranja':
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
    default:
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
        
}
?>

<form method="post" action="./include/procesaFormularioContacto.php">

    <label for="nombre_usuario">Nombre </label>
    <input type="text" id="nombre_usuario" name="nombre" />
    <br/>
    <br/>
    <label for="edad_usuario">Edad</label>
    <input type="text" id="edad_usuario" name="edad" />
    <br/>
    <br/>
    <label id="valoracion">Valora la página </label>
    <input type="number" id="valoracion" name="valoracion" value="" min="0" max="5"/>
    <br/>
    <br/>
    <p id="comentario">¿Quieres hacer algún comentario acerca de la página?</p>
    <label for="comentario"></label>
    <textarea id="comentario" name="comentario" cols="40" rows="1"></textarea>
    <br/>
    <br/>
    <input type="submit" name="enviar" value="¡Envíame!"/>
    <br/>
    <br/>
</form>