<?php
session_start();

$estilos=$_SESSION['tema'];

switch($estilos){
   case 'azul': 
        echo "<link rel=\"stylesheet\" href=\"CSS/azul.css\" />";
        break;
    case 'morado':
        echo "<link rel=\"stylesheet\" href=\"CSS/morado.css\" />";
        break;
    case 'naranja':
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
    default:
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
        
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title></title>
        <link rel="stylesheet" href="../CSS/estilos.css" />
    </head>

    <body>
        <div id="wrapper">
            <header id="cabecera">
                <?php
                include 'cabecera.php';
                ?>
            </header>


            <?php

            if(isset($_POST['tema'])){
                $tema=$_POST['tema'];
                if($tema=='azul'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/azul.css\" />";

                }else if($tema=='morado'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/morado.css\" />";
                }else if($tema=='negro'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/negro.css\" />";
                }else if($tema=='naranja'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/naranja.css\" />";
                }else{
                    echo "Sin valor";
                }
            }

            if(isset($_POST['generos'])){ //isset devuelve un valor true/false
                $generos=$_POST['generos'];
            }else{
                $generos="Sin valor";
            }
            
            $autores=$_POST['autores'];


            if($_POST['autores']==""){
                $autores="Sin valor";
            }
            

            if(isset($_POST['tamano'])){
                $tamano=$_POST['tamano'];
            }else{
                $tamano="Sin valor";
            }

            $libros=$_POST['libros'];


            if($_POST['libros']==""){
                $libros="Sin valor";
            }
            
            
            $titulo=$_POST['titulo'];

            if($_POST['titulo']==""){
                $titulo="Sin valor";
            }

            $opinion_usuario=$_POST['opinion_usuario'];

            if($_POST['opinion_usuario']==""){
                $opinion_usuario="Sin valor";
            }


            echo "<br/>";
            echo "<br/>";
            

            echo "Género/s que has elegido: $generos <br/>";
            echo "Autores/as que has elegido: <br/>";
            
            $cantidadAutores=count($autores);
            for($i=0; $i<$cantidadAutores;$i++){
                if($autores=='Carlos Ruiz Zafón'){
                ?>
                <img class="retratos" src="../imagenes/autores/carlos.jpg" alt="Carlos Ruiz Zafón"/>
                <?php
            }else if($autores=='Virginia Woolf'){
                ?>
                <img class="retratos" src="../imagenes/autores/virginia.jpg" alt="Virginia Woolf"/>
                <?php
            }else if($autores=='Stephen King'){
                ?>
                <img class="retratos" src="../imagenes/autores/stephen.jpg" alt="Stephen King"/>
                <?php
            }else if($autores=='Agatha Christie'){
                ?>
                <img class="retratos" src="../imagenes/autores/agatha.jpg" alt="Agatha Christie"/>
                <?php
            }else if($autores=='H. P. Lovecraft'){
                ?>
                <img class="retratos" src="../imagenes/autores/lovecraft.jpg" alt="H. P. Lovecraft"/>
                <?php
            }else if($autores=='Jane Austen'){
                ?>
                <img class="retratos" src="../imagenes/autores/austen.jpg" alt="Jane Austen"/>
                <?php
            }else if($autores=='Neil Gaiman'){
                ?>
                <img class="retratos" src="../imagenes/autores/neil.jpg" alt="Neil Gaiman"/>
                <?php
            }else if($autores=='Isabel Allende'){
                ?>
                <img class="retratos" src="../imagenes/autores/isabel.jpg" alt="Isabel Allende"/>
                <?php
            }
            }
            
            echo "<br/>";
            echo "Extensión de los libros: $tamano <br/>";
            echo "Te gustaría leer: $libros <br/>";
            echo "<br/>";

            if($libros=='El Hobbit - J.R.R Tolkien'){

            ?>

            <img class="portadas" src="../imagenes/hobbit.jpg" alt="hobbit"/>
            <?php
                //class=\"portadas\"  width=450px height=650px>
            }
            else if($libros=='Harry Potter y la piedra filosofal - J.K.Rowling'){
            ?>
            <img class="portadas" src="../imagenes/potter.jpg" alt="potter" />
            <?php

            }
            else if($libros=='Los hombres que no amaban a las mujeres - Stieg Larsson'){
            ?>
            <img class="portadas" src="../imagenes/millenium.jpg" alt="millenium" />
            <?php

            }  else if($libros=='Viaje al centro de la Tierra - Jules Verne'){
            ?>
            <img class="portadas" src="../imagenes/viaje.jpg" alt="viaje" />
            <?php

            }  else if($libros=='El resplandor - Stephen King'){
            ?>
            <img class="portadas" src="../imagenes/resplandor.jpg" alt="resplandor" />
            <?php

            }  else if($libros=='Fundación - Isaac Asimov'){
            ?>
            <img class="portadas" src="../imagenes/fundacion.jpg" alt="fundacion" />
            <?php

            }  else if($libros=='Los pilares de la tierra - Ken Follet'){
            ?>
            <img class="portadas" src="../imagenes/pilares.jpg" alt="pilares" />
            <?php

            }  else if($libros=='Forastera - Diana Gabaldon'){
            ?>
            <img class="portadas" src="../imagenes/forastera.jpg" alt="forastera" />
            <?php

            }  else if($libros=='El amante japonés - Isabel Allende'){
            ?>
            <img class="portadas" src="../imagenes/japones.jpg" alt="japones" />
            <?php

            }

            echo "<br/>";
            echo "Ya has leído: $titulo <br/>";
            echo "<br/>";
            echo "Y tu opinión es... $opinion_usuario <br/><br/>";

            echo "Este año has leído: ";
            if(isset($_POST['cantidad'])){
                $cantidad=$_POST['cantidad'];
                for($i=0; $i<$cantidad;$i++){
            ?>
            <img src="../imagenes/agenda(1).png" alt="libro"/>
            <?php
                }
            }else{
                $valoracion="Sin valor";   
            }

            echo "</br>";
            echo "</br>";
            echo "</br>";
            
            
            
            if(isset($_POST['comentario'])){
                $comentario=$_POST['comentario'];
                $palabras=explode(" ", $comentario);
                $numeropalabras=count($palabras);
                echo "El número de palabras es: $numeropalabras";
                echo "<ul>";
                echo "Tu comentario: ";
                for($i=0;$i<$numeropalabras;$i++){
                    if($palabras[$i]=="php"){
                        echo "<li class=php>$palabras[$i]</li>";
                    }
                    else{
                        echo "<li class=listaPalabras>$palabras[$i]</li>";
                    }
                }

            }else{
                $comentario="Sin valor";
            }

            echo "</br>";
            echo "<br/>";
            echo "<h3>¡Gracias por colaborar!</h3><br/><br/>";
            ?>

            <a id="volver" href="../indice.php">Volver al índice</a>
            <br/>
            <br/>

        </div>
    </body>
</html>