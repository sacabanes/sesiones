<?php
session_start();

$estilos=$_SESSION['tema'];

switch($estilos){
   case 'azul': 
        echo "<link rel=\"stylesheet\" href=\"CSS/azul.css\" />";
        break;
    case 'morado':
        echo "<link rel=\"stylesheet\" href=\"CSS/morado.css\" />";
        break;
    case 'naranja':
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
    default:
        echo "<link rel=\"stylesheet\" href=\"CSS/naranja.css\" />";
        break;
        
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <title></title>
        <link rel="stylesheet" href="../CSS/estilos.css" />
    </head>

    <body>
        <div id="wrapper">
            <header id="cabecera">
                <?php
                include 'cabecera.php';
                ?>
            </header>


            <?php

            $name=$_POST['nombre'];

            if($_POST['nombre']==""){
                $name="Sin valor";
            }

            $edad=$_POST['edad'];

            if($_POST['edad']==""){
                $edad="Sin valor";
            }

            if(isset($_POST['tema'])){
                $tema=$_POST['tema'];
                if($tema=='azul'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/azul.css\" />";

                }else if($tema=='morado'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/morado.css\" />";
                }else if($tema=='negro'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/negro.css\" />";
                }else if($tema=='naranja'){
                    echo "<link rel=\"stylesheet\" href=\"../CSS/naranja.css\" />";
                }else{
                    echo "Sin valor";
                }
            }


            if(isset($_POST['valoracion'])){
                $valoracion=$_POST['valoracion'];
            }else{
                $valoracion="Sin valor";   
            }

            echo"<br/>";
            echo"<br/>";
            echo "Nombre: $name";
            echo"<br/>";
            echo "Edad: $edad <br/>";

            echo "</br>";
            echo "</br>";
            echo "</br>";

            switch ($valoracion){
                case 0:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_05.png" alt="0"/>
            <?php
                    break;

                case 1:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_15.png" alt="1"/>
            <?php
                    break;

                case 2:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_25.png" alt="2"/>
            <?php
                    break;

                case 3:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_35.png" alt="3"/>
            <?php
                    break;

                case 4:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_45.png" alt="4"/>
            <?php
                    break;

                default:
            ?>
            <img class="estrellas" src="../imagenes/puntuacion/star_55.png" alt="5"/>
            <?php
                    break;
            }
            
            echo "</br>";
            echo "</br>";
            echo "</br>";

            if(isset($_POST['comentario'])){
                $comentario=$_POST['comentario'];
                $palabras=explode(" ", $comentario);
                $numeropalabras=count($palabras);
                echo "El número de palabras es: $numeropalabras";
                echo "<br/>";
                echo "<ul>";
                echo "Tu comentario: ";
                for($i=0;$i<$numeropalabras;$i++){
                    if($palabras[$i]=="php"){
                        echo "<li class=php>$palabras[$i]</li>";
                    }
                    else{
                        echo "<li class=listaPalabras>$palabras[$i]</li>";
                    }
                }
                echo "</ul>";

            }else{
                $comentario="Sin valor";
            }

            echo "</br>";
            echo "</br>";
            echo "</br>";

            echo "<br/>";
            echo "<h3>¡Gracias por colaborar!</h3><br/><br/>";
            ?>

            <a id="volver" href="../indice.php">Volver al índice</a>
            <br/>
            <br/>

        </div>
    </body>
</html>